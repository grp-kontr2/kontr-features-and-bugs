# Kontr Features and Bugs

This repository is only for reporting bugs and features.  
It unifies the place where all the bugs and features can reported without knowing
which Kontr component this bug affects.


## Simple guidelines
For reporting bugs it is always better to report it to the concrete repository.

For example if there is a bug in the Frontend - so the Web Application, 
please report the bug to the 
[frontend repository](https://gitlab.fi.muni.cz/grp-kontr2/portal-frontend/issues).

### List of repositories:

These are not all of the repositories, but this one should cover 
the most of the functionality

- [API Backend](https://gitlab.fi.muni.cz/grp-kontr2/portal/issues) 
(a.k.a the Portal), it is the REST API - the brain that controls everything and 
is connected to the database
- [Web Frontend](https://gitlab.fi.muni.cz/grp-kontr2/portal-frontend/issues)
is how the Web user interface looks like, it is written as single page application
in Angular (current version 6).
- [Kontr API Client](https://gitlab.fi.muni.cz/grp-kontr2/kontr-api/issues) is
the Kontr REST Api client wrapper for the python.
- [Kontrctl](https://gitlab.fi.muni.cz/grp-kontr2/kontrctl/issues) is the CLI tool to
manage the `Kontr portal` using the CLI tools, it is written in python.
- [Kontr Worker](https://gitlab.fi.muni.cz/grp-kontr2/kontr-worker/issues)
is the "executor" for all of the submissions, it runs the submissions inside docker containers.
It communicates with Kontr portal wiht rest API and it is written also in python.
- [KTDK](https://gitlab.fi.muni.cz/grp-kontr2/ktdk/issues) is custom testing 
framework developed in python, it is fully customizable. It is used inside docker containers
to define and run the test scenarios.

### How to create an issue

Steps are quite simple, just click on the `Issues` section in the repository and
then on the `New issue` button.

If you are not sure where does this issue bellongs, just create it here in this repository.

Before creating new issue check whether similar issue has not already been created.


If you are creating the **bug report** please provide us with:
- Description of the bug
- Current behaviour
- Expected behaviour
- Steps to reproduce
- Logs or something that can be usefull to reproduce this issue


If you are creating the **feature request** please provide us with:
- Description
- Proposal
- Expected outcome
- Why you think that this will help or be useful
